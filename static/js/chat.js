window.addEventListener("DOMContentLoaded", function() {
	
	
	let joinChat = document.getElementById("join-chat");
	joinChat.addEventListener("click", join);
	let sendchat = document.getElementById("send-chat");
	console.log(sendchat.innerHTML);
	sendchat.addEventListener("click", chat);
	
	theroom = document.getElementsByTagName('room')[0].attributes[0].nodeValue;
	
});

var theroom = "";

	//socket :)
	var socket = io.connect();

	socket.on( 'connect', function(msg) {
		socket.emit('create', theroom);
	})
	socket.on( 'recvChat', function( msg ) {
			console.log( msg )
			let entry = document.createElement('li');
			let chatlist = document.getElementById("chat-list");
			entry.innerHTML = msg.user_name + ": " + msg.message;
			chatlist.appendChild(entry);
	})
	socket.on( 'recvPlayback', function(playback) {
		isPlaying = playback.playing;
		if(isPlaying == 0) {
			player.pauseVideo();
		} else {
			player.playVideo();
		}
	})

	socket.on( 'recvStatus', function(item) {
		isPlaying = item.playing;
		let clientVid = getYoutubeId(player.getVideoUrl());
		if(item.media !== clientVid) {
			player.cueVideoById(item.media, item.seconds);
		} else {
			player.seekTo(item.seconds, true)
		}

		if(isPlaying == 1)
			player.playVideo();
		else
			player.pauseVideo();

		console.log(item)
	
	})

	socket.on( 'reqStatus', function(item) {
		console.log("I'm the leader and you are being asked for your status")
		socket.emit( 'sendStatus', {
			room : theroom,
			media : getYoutubeId(player.getVideoUrl()),
			seconds: player.getCurrentTime(),
			playing: isPlaying
		  });
	
	})

	socket.on( 'recvServerMsg', function(msg) {
		console.log("new leader! :)" + msg)
		let entry = document.createElement('li');
		let chatlist = document.getElementById("chat-list");
		entry.innerHTML = msg
		entry.classList.add("server-message")
		chatlist.appendChild(entry);
	})

	socket.on( 'reqSeating', function(seating) {
		let container = document.querySelector('.container');
		let room = container.querySelector('.cube');
		let plan = document.querySelector('.plan');
		let seats = [].slice.call(room.querySelectorAll('.row__seat'));
		let planseats = [].slice.call(plan.querySelectorAll('.row__seat'));
		//remove seats
		let takenseats = [].slice.call(document.querySelectorAll('.row__seat--reserved'));
		for(seat of takenseats) {
			
			seat.classList.remove('row__seat--reserved');
			seat.dataset.tooltip = "";
		}
	
		//add seats
		for(seat of seating) {
			if(seat != null) {
				classie.add(planseats[seat.seat], 'row__seat--reserved');
				classie.add(seats[seat.seat], 'row__seat--reserved');
				planseats[seat.seat].dataset.tooltip = seat.name;
			}
		}
	})

	socket.on(	'disconnect', function() {
		$('#disconnect-modal').modal({backdrop: 'static', keyboard: false});
	})

var user_name = "";
function join() {
	socket.emit('roomStatus', theroom);
	socket.emit('reqSeating', theroom);
	let namefield = document.getElementById("name-field");
	user_name = namefield.value;

	socket.emit( 'joinChat', {
		room : theroom,
		user_name : user_name,
	  });
}


function chat() {
	
	if(user_name == null || user_name === "") {
		alert("no name");
		return;
	}
	
	let messageBox = document.getElementById("chat-box");
	let message = messageBox.value;
	messageBox.value = "";

	socket.emit( 'sendChat', {
		room : theroom,
		user_name : user_name,
		message : message
	  });
	
}


