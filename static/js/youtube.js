window.addEventListener("DOMContentLoaded", function() {

	let play = document.getElementById("ctrl-play");
	play.addEventListener("click", playButton);
	
	let next = document.getElementById("ctrl-next");
	next.addEventListener("click", nextButton);
	
	let prev = document.getElementById("ctrl-prev");
	prev.addEventListener("click", prevButton);
	
	let queuebtn = document.getElementById("ctrl-queue-btn");
	queuebtn.addEventListener("click", queue);

	let remqueuebtn = document.getElementById("remove-from-queue");
	remqueuebtn.addEventListener("click", remqueue, remqueuebtn);

	queueItemTemplate = document.getElementsByClassName("video-thumbnail-image")[0].cloneNode(true);

});

var queueItemTemplate = null;

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
player = new YT.Player('player', {
  height: '720',
  width: '1280',
  videoId: 'HUXaV977fiU',
  playerVars: { 'modestbranding': 1, 'rel': 0, 'iv_load_policy': 3, 'loop': 1, 'controls': 1 },
  events: {
	'onReady': onPlayerReady,
	'onStateChange': onPlayerStateChange
  }
});
}
var playerisReady = false;
// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {

	if(!playerisReady) {
		playerisReady = true;
		player.mute();
		socket.emit('reqStatus', theroom);
	}
	
}
var isPlaying = 0;
// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
function onPlayerStateChange(event) {
	let play = document.getElementById("ctrl-play");

	if(player.getPlayerState() == 5) {
		player.playVideo();
	}
	
	if(isPlaying == 0) {
		//player.pauseVideo();
		play.children[0].classList.add("fa-play");
		play.children[0].classList.remove("fa-pause");

	} else {
		//player.playVideo();
		play.children[0].classList.remove("fa-play");
		play.children[0].classList.add("fa-pause");
	}

}


function playButton() {
	if (isPlaying == 0)
		isPlaying = 1;
	else
		isPlaying = 0;

	socket.emit( 'sendPlayback', {
		room: theroom,
		playing : isPlaying,
		media: getYoutubeId(player.getVideoUrl()),
		seconds : player.getCurrentTime()
	  });
}

function nextButton() {
	player.nextVideo();
}

function prevButton() {
	player.previousVideo();
}

function remqueue(btn) {
	console.log(btn);

	btn.target.parentElement.remove();
}

function queue() {
	let ytlink = document.getElementById("ctrl-queue-add-text").value;
	ytlink = getYoutubeId(ytlink);

	if(!ytlink) {
		alert("bad url, try again")
		return;
	}

	socket.emit( 'sendQueue', {
		room : theroom,
		type : "youtube",
		media : ytlink,
		seconds: 0.0,
		playing: isPlaying
	  });


	let qList = document.getElementById("ctrl-queue-list-interface");
	let listing = queueItemTemplate.cloneNode(true);
	listing.children[0].src = "https://img.youtube.com/vi/" + ytlink + "/mqdefault.jpg";

	let remBtn = listing.children[1];
	remBtn.addEventListener("click", remqueue, remBtn);

	qList.appendChild(listing);
	
}



//https://github.com/radiovisual/get-video-id
function getYoutubeId(str) {
	// remove time hash at the end of the string
	str = str.replace(/#t=.*$/, '');

	// shortcode
	var shortcode = /youtube:\/\/|https?:\/\/youtu\.be\/|http:\/\/y2u\.be\//g;

	if (shortcode.test(str)) {
		var shortcodeid = str.split(shortcode)[1];
		return stripParameters(shortcodeid);
	}

	// /v/ or /vi/
	var inlinev = /\/v\/|\/vi\//g;

	if (inlinev.test(str)) {
		var inlineid = str.split(inlinev)[1];
		return stripParameters(inlineid);
	}

	// v= or vi=
	var parameterv = /v=|vi=/g;

	if (parameterv.test(str)) {
		var arr = str.split(parameterv);
		return stripParameters(arr[1].split('&')[0]);
	}

	// v= or vi=
	var parameterwebp = /\/an_webp\//g;

	if (parameterwebp.test(str)) {
		var webp = str.split(parameterwebp)[1];
		return stripParameters(webp);
	}

	// embed
	var embedreg = /\/embed\//g;

	if (embedreg.test(str)) {
		var embedid = str.split(embedreg)[1];
		return stripParameters(embedid);
	}

	// ignore /user/username pattern
	var usernamereg = /\/user\/([a-zA-Z0-9]*)$/g;

	if (usernamereg.test(str)) {
		return undefined;
	}

	// user
	var userreg = /\/user\/(?!.*videos)/g;

	if (userreg.test(str)) {
		var elements = str.split('/');
		return stripParameters(elements.pop());
	}

	// attribution_link
	var attrreg = /\/attribution_link\?.*v%3D([^%&]*)(%26|&|$)/;

	if (attrreg.test(str)) {
		return stripParameters(str.match(attrreg)[1]);
	}
}

function stripParameters(str) {
	// Split parameters or split folder separator
	if (str.indexOf('?') > -1) {
		return str.split('?')[0];
	} else if (str.indexOf('/') > -1) {
		return str.split('/')[0];
	} else if (str.indexOf('&') > -1) {
		return str.split('&')[0];
	}
	return str;
}