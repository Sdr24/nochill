import os, sqlite3, uuid, time
from flask import Flask, request, session, g, jsonify, Markup
from flask import redirect, url_for, render_template, flash
from flask_socketio import SocketIO, join_room, leave_room
from datetime import datetime, timedelta
import key
 


# configure this web application
app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config["SECRET_KEY"] = key.key
socketio = SocketIO(app)


# identify the path to the database file
scriptdir = os.path.dirname(os.path.abspath(__file__))
dbfile = os.path.join(scriptdir, "db/db.sqlite3")

# declare construction and teardown for database
def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(dbfile)
	return db

@app.teardown_appcontext
def close_connection(exception):
	db = getattr(g, '_database', None)
	if db is not None:
		db.close()

@socketio.on('create')
def createRoom(room, methods=['GET', 'POST']):
	try:
		get_db().cursor().execute('INSERT into Media values (?, \"HUXaV977fiU\")', (room,))
		get_db().commit()
	except sqlite3.IntegrityError:
		print(f"new user in room {room}")
	join_room(room)
	join_room(session['id'])

@socketio.on('sendChat')
def chat(chat, methods=['GET', 'POST']):
	socketio.emit('recvChat', chat, to=chat['room'])

@socketio.on('sendPlayback')
def playback(playback, methods=['GET', 'POST']):
	me = get_db().cursor().execute('SELECT leader from Users where salt = ?', (session['id'],)).fetchone()

	#am I the leader?
	if(me != None and me[0] == 1):
		socketio.emit('recvStatus', playback, to=playback['room'])
	else:
		socketio.emit('recvPlayback', playback, to=playback['room'])
		reqStatus(playback['room'])

@socketio.on('joinChat')
def join_chat(data):
	#if first to enter the room
	if get_db().cursor().execute('SELECT count(*) from Users where room = ? and asleep = 0', (data['room'],)).fetchone()[0] == 0:
		leader = 1
	else:
		leader = 0
	
	data['user_name'] = str(Markup(data['user_name']).striptags())
	try:
		get_db().cursor().execute('INSERT into Users (room, name, salt, leader) values (?, ?, ?, ?) ', (data['room'], data['user_name'], session['id'], leader))
	except:
		get_db().cursor().execute('UPDATE Users set asleep = 0 where salt = ?'), (session['id'])
	get_db().commit()

@socketio.on('sendQueue')
def queue(item):
	get_db().cursor().execute('UPDATE Media SET media = ? where room = ?', (item['media'], item['room']))
	get_db().commit()
	socketio.emit('recvStatus', item, to=item['room'])

@socketio.on('sendStatus')
def roomStatus(data):
	socketio.emit('recvStatus', data, to=data['room'])

@socketio.on('sendSeating')
def sendSeating(data):
	get_db().cursor().execute('UPDATE Users set seat = ? where salt = ?', (data['seat'], session['id']))
	get_db().commit()
	reqSeating(data['room'])

@socketio.on('reqSeating')
def reqSeating(room):
	#grab everyone's seat locations
	seating = get_db().cursor().execute('select name, seat from Users where room = ? and asleep = 0', (room,)).fetchall()
	seating = [{'name':seat[0], 'seat':seat[1]} for seat in seating]
	socketio.emit('reqSeating', seating, to=room)

@socketio.on('reqStatus')
def reqStatus(room):
	try:
		leader = get_db().cursor().execute('SELECT salt from Users where room = ?', (room,)).fetchone()[0]
		socketio.emit('reqStatus', None, to=leader)
	except Exception:
		print("new room")

@socketio.on('disconnect')
def disconnect():
	get_db().cursor().execute('UPDATE Users set asleep = 1, leader = 0 where salt = ?', (session['id'],))
	get_db().commit()
	#do we need a new leader?
	activeLeaders = get_db().cursor().execute('SELECT count(*) from Users where leader = 1 and asleep = 0 and room = (select room from Users where salt = ?)', (session['id'],)).fetchone()[0]
	if(activeLeaders < 1):
		#we need a new leader
		try:
			newLeader = get_db().cursor().execute("Select salt from Users where room = (select room from Users where salt = ?) and asleep = 0 limit 1", (session['id'],)).fetchone()[0]
			get_db().cursor().execute('UPDATE Users set leader = 1 where salt = ?', (newLeader,))
			socketio.emit('recvServerMsg', "You are the new leader", to=newLeader)
			get_db().commit()
		except:
			get_db().cursor().execute('DELETE from Media where room = (select room from Users where salt = ?)', (session['id'],))
			get_db().commit()
			print("Room closed")

# --------------
# ROUTE HANDLERS
# --------------

@app.route("/")
def index():
	rooms = get_db().cursor().execute('select room, media, cnt from Media join (select room, count(*) as cnt from Users where asleep = 0 group by room) using(room) order by cnt desc').fetchall()
	rooms = [{'room':room[0], 'media':room[1], 'cnt':room[2]} for room in rooms]
	return render_template("home.html", rooms=rooms)

# load theater
@app.route("/<string:name>/")
def room(name):
	session['id'] = str(uuid.uuid4())
	return render_template("room.html", name=name)

@app.before_first_request
def cleanDB():
	get_db().cursor().execute('DROP table Users')
	get_db().cursor().execute('DROP table Media')

	get_db().cursor().execute('CREATE TABLE Media (room TEXT not null, media TEXT null, primary key (room))')
	get_db().cursor().execute('''
		CREATE TABLE "Users" (
		"room" TEXT NOT NULL,
		"name" TEXT NOT NULL,
		"salt" TEXT NOT NULL,
		"seat" INTEGER,
		"leader" INTEGER,
		"asleep" INTEGER NOT NULL DEFAULT 0
		)
	''')
	get_db().commit()
	
if __name__ == '__main__':
	socketio.run(app, debug=True)