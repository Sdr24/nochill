# nochill
[![screenshot](https://gitlab.com/Sdr24/nochill/-/raw/master/img.png "screenshot")](https://gitlab.com/Sdr24/nochill/-/raw/master/img.png "screenshot")
This is a clone of the critically acclaimed andchill.tv that actually works. I don\'t really want to host it myself and my design is not that great so feel free to spin up an instance for everyone/just your friends.
   
Pretty much the entire frontend was created by Mary Lou as a cinema seat simulator. [Github link](https://github.com/codrops/SeatPreview)
  
Under the hood we\'ve got a Flask server with SQLite3 for the database and websockets for all the cool stuff.
## Setting it up
#### Requirements
```console
$ python -m pip install Flask
```
#### Secret Key
Before running the server, you must define a secret key. To do this, you must create a `key.py` file in the same directory as `app.py` with the format
```python
key="blahblahblah"
```
#### Running the server
The simplest way to get the wheels turning is to use Flask\'s bundled development server. `cd` your way to the directory with app.py and run
```console
$ python app.py
```
For production environments, you would do well to use something more suited for the task, such as wsgi. 
## Contributing
If you want to send a pull request that\'s cool do it